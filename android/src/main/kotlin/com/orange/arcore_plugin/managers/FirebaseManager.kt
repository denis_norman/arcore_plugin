package com.orange.arcore_plugin.managers

import android.content.Context
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.orhanobut.logger.Logger

class FirebaseManager(context: Context) {
    companion object {
        private const val KEY_ROOT_DIR = "shared_anchor_codelab_root"
        private const val KEY_NEXT_SHORT_CODE = "next_short_code"
        private const val KEY_PREFIX = "anchor;"
        private const val INITIAL_SHORT_CODE = 142
    }

    private val rootRef: DatabaseReference

    init {
        val firebaseApp = FirebaseApp.initializeApp(context)
        rootRef = FirebaseDatabase.getInstance(firebaseApp!!).reference.child(KEY_ROOT_DIR)
        DatabaseReference.goOnline()
    }

    fun nextShortCode(listener: (Int?) -> Unit) {
        rootRef.child(KEY_NEXT_SHORT_CODE).runTransaction(
                object : Transaction.Handler {
                    override fun doTransaction(currentData: MutableData): Transaction.Result {
                        var shortCode = currentData.getValue(Int::class.java)
                        if (shortCode == null) {
                            shortCode = INITIAL_SHORT_CODE - 1
                        }
                        currentData.value = shortCode + 1
                        return Transaction.success(currentData)
                    }

                    override fun onComplete(
                            error: DatabaseError?, committed: Boolean, currentData: DataSnapshot?) {
                        if (!committed) {
                            Logger.e("$error")
                            listener(null)
                        } else {
                            listener(currentData!!.getValue(Int::class.java))
                        }
                    }
                }
        )
    }

    fun storeUsingShortCode(shortCode: Int, cloudAnchorId: String) {
        rootRef.child(KEY_PREFIX + shortCode).setValue(cloudAnchorId)
    }

    fun getCloudAnchorId(shortCode: Int, listener: (String?) -> Unit) {
        rootRef.child(KEY_PREFIX + shortCode).addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        listener(dataSnapshot.value.toString())
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Logger.e("$error")
                        listener(null)
                    }
                }
        )
    }
}
