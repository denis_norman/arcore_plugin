package com.orange.arcore_plugin.managers

import com.google.ar.core.Anchor
import com.google.ar.core.Anchor.CloudAnchorState
import com.google.ar.core.Anchor.CloudAnchorState.NONE
import com.google.ar.core.Anchor.CloudAnchorState.TASK_IN_PROGRESS
import com.google.ar.core.Session
import java.util.*

class CloudAnchorManager {
    private val pendingAnchors = HashMap<Anchor, (Anchor) -> Unit>()

    @Synchronized
    fun hostCloudAnchor(
            session: Session, anchor: Anchor, listener: (Anchor) -> Unit) {
        val newAnchor = session.hostCloudAnchor(anchor)
        pendingAnchors[newAnchor] = listener
    }

    @Synchronized
    fun resolveCloudAnchor(
            session: Session, anchorId: String, listener: (Anchor) -> Unit) {
        val newAnchor = session.resolveCloudAnchor(anchorId)
        pendingAnchors[newAnchor] = listener
    }

    @Synchronized
    fun onUpdate() {
        val iter = pendingAnchors.entries.iterator()
        while (iter.hasNext()) {
            val entry = iter.next()
            val anchor = entry.key
            if (isReturnableState(anchor.cloudAnchorState)) {
                val listener = entry.value
                listener(anchor)
                iter.remove()
            }
        }
    }

    @Synchronized
    fun clearListeners() {
        pendingAnchors.clear()
    }

    private fun isReturnableState(cloudState: CloudAnchorState): Boolean {
        return when (cloudState) {
            NONE, TASK_IN_PROGRESS -> false
            else -> true
        }
    }
}
