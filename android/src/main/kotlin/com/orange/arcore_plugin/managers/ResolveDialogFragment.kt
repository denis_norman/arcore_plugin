package com.orange.arcore_plugin.managers

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams

class ResolveDialogFragment : DialogFragment() {
    companion object {
        private const val MAX_FIELD_LENGTH = 6

        fun createWithOkListener(listener: (Int) -> Unit): ResolveDialogFragment {
            val frag = ResolveDialogFragment()
            frag.okListener = listener
            return frag
        }
    }

    private var shortCodeField: EditText? = null
    private var okListener: ((Int) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder
                .setView(createDialogLayout())
                .setTitle("Resolve Anchor")
                .setPositiveButton("Resolve") { _, _ -> onResolvePressed() }
                .setNegativeButton("Cancel") { _, _ -> }
        return builder.create()
    }

    private fun createDialogLayout(): LinearLayout {
        val context = context
        val layout = LinearLayout(context)
        shortCodeField = EditText(context)
        shortCodeField!!.inputType = InputType.TYPE_CLASS_NUMBER
        shortCodeField!!.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        shortCodeField!!.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(MAX_FIELD_LENGTH))
        layout.addView(shortCodeField)
        layout.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        return layout
    }

    private fun onResolvePressed() {
        val roomCodeText = shortCodeField!!.text
        if (okListener != null && roomCodeText != null && roomCodeText.isNotEmpty()) {
            val longVal = Integer.parseInt(roomCodeText.toString())
            okListener!!(longVal)
        }
    }
}
