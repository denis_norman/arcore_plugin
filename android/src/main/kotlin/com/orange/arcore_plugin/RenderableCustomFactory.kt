package com.orange.arcore_plugin

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.widget.Toast
import com.google.ar.sceneform.assets.RenderableSource
import com.google.ar.sceneform.assets.RenderableSource.RecenterMode.ROOT
import com.google.ar.sceneform.assets.RenderableSource.SourceType.GLTF2
import com.google.ar.sceneform.rendering.Material
import com.google.ar.sceneform.rendering.ModelRenderable
import com.orange.arcore_plugin.models.FlutterArCoreNode
import com.orhanobut.logger.Logger


typealias MaterialHandler = (Material?, Throwable?) -> Unit
typealias RenderableHandler = (ModelRenderable?, Throwable?) -> Unit

class RenderableCustomFactory {
    companion object {
        @SuppressLint("ShowToast")
        fun makeRenderable(context: Context, flutterArCoreNode: FlutterArCoreNode, handler: RenderableHandler) {
            if (flutterArCoreNode.dartType == "ArCoreReferenceNode") {
                val url = flutterArCoreNode.objectUrl
                val localObject = flutterArCoreNode.obcject3DFileName
                Logger.i(localObject ?: "")
                if (localObject != null) {
                    val builder = ModelRenderable.builder()
                    builder.setSource(context, Uri.parse(localObject))
                    builder.build().thenAccept { renderable ->
                        handler(renderable, null)
                    }.exceptionally { throwable ->
                        Logger.e("Unable to load Renderable.", throwable)
                        handler(null, throwable)
                        return@exceptionally null
                    }
                } else if (url != null) {
                    val modelRenderableBuilder = ModelRenderable.builder()
                    val renderableSourceBuilder = RenderableSource.builder()

                    renderableSourceBuilder
                            .setSource(context, Uri.parse(url), GLTF2)
                            .setScale(0.5f)
                            .setRecenterMode(ROOT)

                    modelRenderableBuilder
                            .setSource(context, renderableSourceBuilder.build())
                            .setRegistryId(url)
                            .build()
                            .thenAccept { renderable ->
                                handler(renderable, null)
                            }
                            .exceptionally { throwable ->
                                handler(null, throwable)
                                Logger.i("renderable error ${throwable.localizedMessage}")
                                null
                            }
                }
            } else {
                makeMaterial(context, flutterArCoreNode) { material, _ ->
                    if (material != null) {
                        Logger.i("material not null")
                        try {
                            val renderable = flutterArCoreNode.shape?.buildShape(material)
                            handler(renderable, null)
                        } catch (ex: Exception) {
                            Logger.i("renderable error $ex")
                            handler(null, ex)
                            Toast.makeText(context, ex.toString(), Toast.LENGTH_LONG)
                        }
                    }
                }
            }
        }

        private fun makeMaterial(context: Context, flutterArCoreNode: FlutterArCoreNode, handler: MaterialHandler) {
            val texture = flutterArCoreNode.shape?.materials?.first()?.texture
            val color = flutterArCoreNode.shape?.materials?.first()?.color
            if (texture != null) {
                val isPng = texture.endsWith("png")
                val builder = com.google.ar.sceneform.rendering.Texture.builder()
                builder.setSource(context, Uri.parse(texture))
                builder.build().thenAccept {
                    MaterialCustomFactory.makeWithTexture(context, it, isPng, flutterArCoreNode.shape.materials[0])?.thenAccept { material ->
                        handler(material, null)
                    }?.exceptionally { throwable ->
                        Logger.i("texture error $throwable")
                        handler(null, throwable)
                        return@exceptionally null
                    }
                }
            } else if (color != null) {
                MaterialCustomFactory.makeWithColor(context, flutterArCoreNode.shape.materials[0])
                        ?.thenAccept { material: Material ->
                            handler(material, null)
                        }?.exceptionally { throwable ->
                            Logger.i("material error $throwable")
                            handler(null, throwable)
                            return@exceptionally null
                        }
            }
        }
    }
}