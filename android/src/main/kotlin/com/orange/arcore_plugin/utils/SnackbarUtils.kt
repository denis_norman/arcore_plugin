package com.orange.arcore_plugin.utils

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.Gravity.*
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.makeText
import com.orhanobut.logger.Logger

class SnackbarUtils {
    companion object {
        fun showMessage(context: Context, message: String) {
            Handler(Looper.getMainLooper())
                    .post {
                        val toast = makeText(context, message, LENGTH_LONG)
                        toast.setGravity(BOTTOM, 0, 0)
                        toast.show()
                    }
        }

        fun showError(
                context: Context, errorMsg: String, problem: Throwable?) {
            val toastText: String
            toastText = when {
                problem?.message != null -> {
                    Logger.e(errorMsg, problem)
                    errorMsg + ": " + problem.message
                }
                problem != null -> {
                    Logger.e(errorMsg, problem)
                    errorMsg
                }
                else -> {
                    Logger.e(errorMsg)
                    errorMsg
                }
            }
            Handler(Looper.getMainLooper())
                    .post {
                        val toast = makeText(context, toastText, LENGTH_LONG)
                        toast.setGravity(BOTTOM, 0, 0)
                        toast.show()
                    }
        }
    }
}