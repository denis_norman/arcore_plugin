package com.orange.arcore_plugin.utils

import android.Manifest.permission.CAMERA
import android.app.Activity
import android.app.ActivityManager
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.net.Uri.fromParts
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.N
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.widget.Toast.LENGTH_LONG
import android.widget.Toast.makeText
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat.checkSelfPermission
import com.google.ar.core.ArCoreApk.InstallStatus.INSTALLED
import com.google.ar.core.ArCoreApk.InstallStatus.INSTALL_REQUESTED
import com.google.ar.core.ArCoreApk.getInstance
import com.google.ar.core.Config
import com.google.ar.core.Config.CloudAnchorMode.ENABLED
import com.google.ar.core.Config.UpdateMode.LATEST_CAMERA_IMAGE
import com.google.ar.core.Session
import com.google.ar.core.exceptions.*
import com.orhanobut.logger.Logger
import java.lang.Double.parseDouble

class ArCoreUtils {


    companion object {
        private const val MIN_OPENGL_VERSION = 3.0
        private const val CAMERA_PERMISSION_CODE = 0
        private const val CAMERA_PERMISSION = CAMERA

        @Throws(UnavailableException::class)
        fun createArSession(activity: Activity, installRequested: Boolean): Session? {
            var session: Session? = null
            if (hasCameraPermission(activity)) {
                when (getInstance().requestInstall(activity, !installRequested)) {
                    INSTALL_REQUESTED -> return null
                    else -> {
                    }
                }
                session = Session(activity)
                val config = Config(session)
                config.updateMode = LATEST_CAMERA_IMAGE
                config.cloudAnchorMode = ENABLED
                session.configure(config)
            }
            return session
        }

        fun requestCameraPermission(activity: Activity, requestCode: Int) {
            requestPermissions(
                    activity, arrayOf(CAMERA), requestCode)
        }

        fun hasCameraPermission(activity: Activity): Boolean {
            return checkSelfPermission(activity, CAMERA) == PERMISSION_GRANTED
        }

        fun shouldShowRequestPermissionRationale(activity: Activity): Boolean {
            return shouldShowRequestPermissionRationale(
                    activity, CAMERA)
        }

        fun launchPermissionSettings(activity: Activity) {
            val intent = Intent()
            intent.action = ACTION_APPLICATION_DETAILS_SETTINGS
            intent.data = fromParts("package", activity.packageName, null)
            activity.startActivity(intent)
        }

        fun handleSessionException(
                activity: Activity, sessionException: UnavailableException) {
            val message = when (sessionException) {
                is UnavailableArcoreNotInstalledException -> "Please install ARCore"
                is UnavailableApkTooOldException -> "Please update ARCore"
                is UnavailableSdkTooOldException -> "Please update this app"
                is UnavailableDeviceNotCompatibleException -> "This device does not support AR"
                else -> {
                    Logger.e("Exception: $sessionException")
                    "Failed to create AR session"
                }
            }
            makeText(activity, message, LENGTH_LONG).show()
        }

        fun checkIsSupportedDeviceOrFinish(activity: Activity): Boolean {
            if (SDK_INT < N) {
                Logger.e("Sceneform requires Android N or later")
                makeText(activity, "Sceneform requires Android N or later", LENGTH_LONG).show()
                activity.finish()
                return false
            }
            val openGlVersionString = (activity.getSystemService(ACTIVITY_SERVICE) as ActivityManager)
                    .deviceConfigurationInfo
                    .glEsVersion
            if (parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
                Logger.e("Sceneform requires OpenGL ES 3.0 later")
                makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", LENGTH_LONG)
                        .show()
                activity.finish()
                return false
            }
            return true
        }

        fun checkIfArCoreIsInstalled(activity: Activity, installRequested: Boolean): Boolean {
            return when (getInstance().requestInstall(activity,
                    installRequested)) {
                INSTALL_REQUESTED -> true
                else -> false
            }
        }
    }
}
