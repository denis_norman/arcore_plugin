@file:Suppress("UNCHECKED_CAST")

package com.orange.arcore_plugin

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application.ActivityLifecycleCallbacks
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.google.ar.core.Anchor
import com.google.ar.core.Anchor.CloudAnchorState.SUCCESS
import com.google.ar.core.Frame
import com.google.ar.core.Plane
import com.google.ar.core.Pose
import com.google.ar.core.TrackingState.TRACKING
import com.google.ar.core.exceptions.CameraNotAvailableException
import com.google.ar.core.exceptions.UnavailableException
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.HitTestResult
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.Scene.OnUpdateListener
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.FootprintSelectionVisualizer
import com.google.ar.sceneform.ux.SelectionVisualizer
import com.google.ar.sceneform.ux.TransformableNode
import com.google.ar.sceneform.ux.TransformationSystem
import com.orange.arcore_plugin.managers.CloudAnchorManager
import com.orange.arcore_plugin.managers.FirebaseManager
import com.orange.arcore_plugin.managers.ResolveDialogFragment
import com.orange.arcore_plugin.models.FlutterArCoreHitTestResult
import com.orange.arcore_plugin.models.FlutterArCoreNode
import com.orange.arcore_plugin.models.FlutterArCorePose
import com.orange.arcore_plugin.models.RotatingNode
import com.orange.arcore_plugin.utils.ArCoreUtils
import com.orange.arcore_plugin.utils.DecodableUtils.Companion.parseVector3
import com.orange.arcore_plugin.utils.SnackbarUtils
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.platform.PlatformView

@SuppressLint("ValidFragment")
class ArCoreView(_context: Context, _messenger: BinaryMessenger, _id: Int) :
        PlatformView, MethodCallHandler {

    // region Field

    companion object {
        private const val RC_PERMISSIONS = 0x123
    }

    private lateinit var activityLifecycleCallbacks: ActivityLifecycleCallbacks
    private lateinit var modelRenderable: ModelRenderable
    private var anchorNode: AnchorNode?
    private var installRequested: Boolean
    private val methodChannel: MethodChannel
    private val activity: Activity
    private val _arSceneView: ArSceneView
    private val gestureDetector: GestureDetector
    private val cloudAnchorListener: OnUpdateListener
    private val sceneUpdateListener: OnUpdateListener
    private val cloudAnchorManager: CloudAnchorManager
    private val firebaseManager: FirebaseManager

    // endregion

    init {
        Logger.addLogAdapter(AndroidLogAdapter())
        cloudAnchorManager = CloudAnchorManager()
        installRequested = false
        anchorNode = null
        activity = (_context.applicationContext as FlutterApplication).currentActivity
        firebaseManager = FirebaseManager(activity)
        methodChannel = MethodChannel(_messenger, "arcore_flutter_plugin_$_id")
        methodChannel.setMethodCallHandler(this)
        _arSceneView = ArSceneView(activity)
        cloudAnchorListener = OnUpdateListener {
            cloudAnchorManager.onUpdate()
        }
        gestureDetector = GestureDetector(
                activity,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onSingleTapUp(e: MotionEvent): Boolean {
                        onSingleTap(e)
                        return true
                    }

                    override fun onDown(e: MotionEvent): Boolean {
                        return true
                    }
                })
        sceneUpdateListener = OnUpdateListener {
            val frame = _arSceneView.arFrame ?: return@OnUpdateListener
            if (frame.camera.trackingState != TRACKING) {
                return@OnUpdateListener
            }
            for (plane in frame.getUpdatedTrackables(Plane::class.java)) {
                if (plane.trackingState == TRACKING) {
                    val pose = plane.centerPose
                    val map: HashMap<String, Any> = HashMap()
                    map["type"] = plane.type.ordinal
                    map["centerPose"] = FlutterArCorePose(pose.translation, pose.rotationQuaternion).toHashMap()
                    map["extentX"] = plane.extentX
                    map["extentZ"] = plane.extentZ
                    methodChannel.invokeMethod("onPlaneDetected", map)

                }
            }
        }
        ArCoreUtils.requestCameraPermission(activity, RC_PERMISSIONS)
        setupLifeCycle(activity)
        val localObject = "Andy.sfb"
        val builder = ModelRenderable.builder()
        builder.setSource(_context, Uri.parse(localObject))
        builder.build().thenAccept { renderable ->
            modelRenderable = renderable
        }
    }

    private fun setupLifeCycle(context: Context) {
        activityLifecycleCallbacks = object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                Logger.i("onActivityCreated")
            }

            override fun onActivityStarted(activity: Activity) {
                Logger.i("onActivityStarted")
            }

            override fun onActivityResumed(activity: Activity) {
                Logger.i("onActivityResumed")
                onResume()
            }

            override fun onActivityPaused(activity: Activity) {
                Logger.i("onActivityPaused")
                onPause()
            }

            override fun onActivityStopped(activity: Activity) {
                Logger.i("onActivityStopped")
                onPause()
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

            override fun onActivityDestroyed(activity: Activity) {
                Logger.i("onActivityDestroyed")
                onDestroy()
            }
        }
        (context.applicationContext as FlutterApplication).currentActivity.application
                .registerActivityLifecycleCallbacks(this.activityLifecycleCallbacks)
    }

    @Synchronized
    override fun onMethodCall(call: MethodCall, result: Result) {
        when (call.method) {
            "init" -> {
                arScenViewInit(call, result)
            }
            "addArCoreNode" -> {
                Logger.i("addArCoreNode")
                val map = call.arguments as HashMap<String, Any>
                val flutterNode = FlutterArCoreNode(map)
                onAddNode(flutterNode, result)
            }
            "addArCoreNodeWithAnchor" -> {
                Logger.i("addArCoreNodeWithAnchor")
                val map = call.arguments as HashMap<String, Any>
                val flutterNode = FlutterArCoreNode(map)
//                addNodeWithAnchor(flutterNode, result)
                onArPlaneTap(flutterNode, result)
            }
            "removeARCoreNode" -> {
                Logger.i("removeARCoreNode")
                val map = call.arguments as HashMap<String, Any>
                removeNode(map["nodeName"] as String, result)
            }
            "positionChanged" -> {
                Logger.i("positionChanged")

            }
            "rotationChanged" -> {
                Logger.i("rotationChanged")
                updateRotation(call, result)

            }
            "updateMaterials" -> {
                Logger.i("updateMaterials")
                updateMaterials(call, result)

            }
            "dispose" -> {
                Logger.i("updateMaterials")
                dispose()
            }
            "resolveAnchor" -> {
                Logger.i("resolveAnchor")
                resolveAnchor()
            }
            else -> {
            }
        }
    }

    fun onResume() {
        if (!ArCoreUtils.hasCameraPermission(activity)) {
            ArCoreUtils.requestCameraPermission(activity, RC_PERMISSIONS)
        }
        if (_arSceneView.session == null) {
            try {
                val session = ArCoreUtils.createArSession(activity, installRequested)
                if (session == null) {
                    installRequested = ArCoreUtils.hasCameraPermission(activity)
                    return
                } else {
                    _arSceneView.setupSession(session)
                }
            } catch (e: UnavailableException) {
                ArCoreUtils.handleSessionException(activity, e)
            }
        }
        try {
            _arSceneView.resume()
        } catch (ex: CameraNotAvailableException) {
            SnackbarUtils.showError(activity, "Unable to get camera", ex)
            activity.finish()
            return
        }
        if (_arSceneView.session != null) {
            Logger.i("Searching for surfaces")
        }
        if (_arSceneView.scene != null) {
            _arSceneView.scene?.addOnUpdateListener(cloudAnchorListener)
        }
    }

    @Synchronized
    private fun onShortCodeEntered(shortCode: Int) {
        firebaseManager.getCloudAnchorId(shortCode) { cloudAnchorId ->
            if (cloudAnchorId == null || cloudAnchorId.isEmpty()) {
                SnackbarUtils.showMessage(
                        activity,
                        "A Cloud Anchor ID for the short code $shortCode was not found."
                )
            }
            cloudAnchorManager.resolveCloudAnchor(_arSceneView.session!!,
                    cloudAnchorId!!
            ) { anchor ->
                onResolvedAnchorAvailable(anchor, shortCode)
            }
        }
    }

    @Synchronized
    private fun onResolvedAnchorAvailable(anchor: Anchor, shortCode: Int) {
        val cloudState = anchor.cloudAnchorState
        if (cloudState == SUCCESS) {
            SnackbarUtils.showMessage(activity, "Cloud Anchor Resolved. Short Code: $shortCode")
            setNewAnchor(anchor)
        } else {
            SnackbarUtils.showMessage(
                    activity,
                    "Error while resolving anchor with short code "
                            + shortCode
                            + ". Error: "
                            + cloudState.toString()
            )
        }
    }

    private fun resolveAnchor() {
        val dialog = ResolveDialogFragment.createWithOkListener(::onShortCodeEntered)
        dialog.show(activity.fragmentManager, "Resolve")
    }

    @Synchronized
    private fun onHostedAnchorAvailable(anchor: Anchor) {
        val cloudState = anchor.cloudAnchorState
        if (cloudState == SUCCESS) {
            val cloudAnchorId = anchor.cloudAnchorId
            firebaseManager.nextShortCode { shortCode ->
                if (shortCode != null) {
                    firebaseManager.storeUsingShortCode(shortCode, cloudAnchorId)
                    SnackbarUtils
                            .showMessage(activity, "Cloud Anchor Hosted. Short code: $shortCode")
                } else {
                    SnackbarUtils
                            .showMessage(activity, "Cloud Anchor Hosted, but could not get a short code from Firebase.")
                }
            }
            setNewAnchor(anchor)
        } else {
            Logger.e("Error while hosting: $cloudState")
        }
    }

    private fun setNewAnchor(anchor: Anchor) {
        if (anchorNode != null) {
            _arSceneView.scene.removeChild(anchorNode)
            anchorNode = null
        }
        anchorNode = AnchorNode(anchor)
        _arSceneView.scene.addChild(anchorNode)
        val selectionVisualizer: SelectionVisualizer = FootprintSelectionVisualizer()
        val transformationSystem = TransformationSystem(activity.resources.displayMetrics!!,
                selectionVisualizer)
        val transformableNode = TransformableNode(transformationSystem)
        transformableNode.setParent(anchorNode)
        transformableNode.renderable = modelRenderable
        transformableNode.select()
    }

    @Synchronized
    private fun onArPlaneTap(node: FlutterArCoreNode, result: Result) {
        if (anchorNode != null) {
            return
        }
        val pose = Pose(node.getPosition(), node.getRotation())
        val anchor = _arSceneView.session!!.createAnchor(pose)
        setNewAnchor(anchor)
        SnackbarUtils.showMessage(activity, "Now hosting anchor...")
        cloudAnchorManager.hostCloudAnchor(
                _arSceneView.session!!, anchor, ::onHostedAnchorAvailable)
        result.success(null)
    }

//    @Synchronized
//    private fun createAnchorNode(anchor: Anchor, renderable: ModelRenderable) {
//        val anchorNode = AnchorNode(anchor)
//        anchorNode.name = arCoreNode.name
//        anchorNode.renderable = renderable
//        attachNodeToParent(anchorNode, arCoreNode.parentNodeName)
//        for (node in arCoreNode.children) {
//            node.parentNodeName = arCoreNode.name
//            onAddNode(node, null)
//        }
//        Logger.i("Inserted ${anchorNode.name}")
//    }

//    @Synchronized
//    private fun addNodeWithAnchor(flutterArCoreNode: FlutterArCoreNode,
//                                  result: Result) {
//        arCoreNode = flutterArCoreNode
//        RenderableCustomFactory.makeRenderable(activity.applicationContext, flutterArCoreNode) { renderable, _ ->
//            if (renderable != null) {
//                val pose = Pose(flutterArCoreNode.getPosition(), flutterArCoreNode.getRotation())
//                val anchor = _arSceneView.session?.createAnchor(pose)
//                if (anchor != null) {
//                    createAnchorNode(anchor, renderable)
//                    cloudAnchorManager.hostCloudAnchor(_arSceneView.session!!, anchor, ::onHostedAnchorAvailable)
//                }
//            }
//        }
//        result.success(null)
//    }

    private fun arScenViewInit(call: MethodCall, result: Result) {
        Logger.i("arScenViewInit")
        onResume() //TODO delete?
        val enableTapRecognizer: Boolean? = call.argument("enableTapRecognizer")
        if (enableTapRecognizer != null && enableTapRecognizer) {
            _arSceneView.scene
                    ?.setOnTouchListener { hitTestResult: HitTestResult, event: MotionEvent? ->

                        if (hitTestResult.node != null) {
                            Logger.i(" onNodeTap " + hitTestResult.node?.name)
                            Logger.i(hitTestResult.node?.localPosition.toString())
                            Logger.i(hitTestResult.node?.worldPosition.toString())
                            methodChannel.invokeMethod("onNodeTap", hitTestResult.node?.name)
                            return@setOnTouchListener true
                        }
                        return@setOnTouchListener gestureDetector.onTouchEvent(event)
                    }
        }
        val enableUpdateListener: Boolean? = call.argument("enableUpdateListener")
        if (enableUpdateListener != null && enableUpdateListener) {
            _arSceneView.scene?.addOnUpdateListener(sceneUpdateListener)
        }
        result.success(null)
    }

    private fun tryPlaceNode(tap: MotionEvent?, frame: Frame) {
        if (tap != null && frame.camera.trackingState == TRACKING) {
            for (hit in frame.hitTest(tap)) {
                val trackable = hit.trackable
                if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                    // Create the Anchor.
                    val anchor = hit.createAnchor()
                    val anchorNode = AnchorNode(anchor)
                    anchorNode.setParent(_arSceneView.scene)
                    ModelRenderable.builder()
                            .setSource(activity.applicationContext, Uri.parse("TocoToucan.sfb"))
                            .build()
                            .thenAccept { renderable ->
                                val node = Node()
                                node.renderable = renderable
                                anchorNode.addChild(node)
                            }.exceptionally { throwable ->
                                Logger.e("Unable to load Renderable.", throwable)
                                return@exceptionally null
                            }
                }
            }
        }

    }

    private fun onSingleTap(tap: MotionEvent?) {
        Logger.i(" onSingleTap")
        val frame = _arSceneView.arFrame
        if (frame != null) {
            if (tap != null && frame.camera.trackingState == TRACKING) {
                val hitList = frame.hitTest(tap)
                val list = ArrayList<HashMap<String, Any>>()
                for (hit in hitList) {
                    val trackable = hit.trackable
                    if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                        hit.hitPose
                        val distance: Float = hit.distance
                        val translation = hit.hitPose.translation
                        val rotation = hit.hitPose.rotationQuaternion
                        val flutterArCoreHitTestResult = FlutterArCoreHitTestResult(distance, translation, rotation)
                        val arguments = flutterArCoreHitTestResult.toHashMap()
                        list.add(arguments)
                    }
                }
                methodChannel.invokeMethod("onPlaneTap", list)
            }
        }
    }

    private fun onAddNode(flutterArCoreNode: FlutterArCoreNode, result: Result?) {
        Logger.i("onAddNode")
        NodeFactory.makeNode(activity.applicationContext, flutterArCoreNode) { node, _ ->
            Logger.i("inserted ${node?.name}")
            if (node != null) {
                attachNodeToParent(node, flutterArCoreNode.parentNodeName)
                for (n in flutterArCoreNode.children) {
                    n.parentNodeName = flutterArCoreNode.name
                    onAddNode(n, null)
                }
            }

        }
        result?.success(null)
    }

    private fun attachNodeToParent(node: Node?, parentNodeName: String?) {
        if (parentNodeName != null) {
            Logger.i(parentNodeName)
            val parentNode: Node? = _arSceneView.scene?.findByName(parentNodeName)
            parentNode?.addChild(node)
        } else {
            Logger.i("addNodeToSceneWithGeometry: NOT PARENT_NODE_NAME")
            _arSceneView.scene?.addChild(node)
        }
    }

    private fun removeNode(name: String, result: Result) {
        val node = _arSceneView.scene?.findByName(name)
        if (node != null) {
            _arSceneView.scene?.removeChild(node)
            Logger.i("removed ${node.name}")
        }
        result.success(null)
    }

    private fun updatePosition(call: MethodCall, result: Result) {
        val name = call.argument<String>("name")
        val node = _arSceneView.scene?.findByName(name)
        node?.localPosition = parseVector3(call.arguments as HashMap<String, Any>)
        result.success(null)
    }

    private fun updateRotation(call: MethodCall, result: Result) {
        val name = call.argument<String>("name")
        val node = _arSceneView.scene?.findByName(name) as RotatingNode
        Logger.i("rotating node:  $node")
        val degreesPerSecond = call.argument<Double?>("degreesPerSecond")
        Logger.i("rotating value:  $degreesPerSecond")
        if (degreesPerSecond != null) {
            Logger.i("rotating value:  ${node.degreesPerSecond}")
            node.degreesPerSecond = degreesPerSecond.toFloat()
        }
        result.success(null)
    }

    private fun updateMaterials(call: MethodCall, result: Result) {
        val name = call.argument<String>("name")
        val materials = call.argument<ArrayList<HashMap<String, *>>>("materials")!!
        val node = _arSceneView.scene?.findByName(name)
        val oldMaterial = node?.renderable?.material?.makeCopy()
        if (oldMaterial != null) {
            val material = MaterialCustomFactory.updateMaterial(oldMaterial, materials[0])
            node.renderable?.material = material
        }
        result.success(null)
    }

    override fun getView(): View {
        return _arSceneView
    }

    override fun dispose() {
        onPause()
        onDestroy()
    }

    fun onPause() {
        _arSceneView.pause()
    }

    fun onDestroy() {
        cloudAnchorManager.clearListeners()
        _arSceneView.scene?.removeOnUpdateListener(sceneUpdateListener)
        _arSceneView.scene?.removeOnUpdateListener(cloudAnchorListener)
        _arSceneView.destroy()
    }
}





