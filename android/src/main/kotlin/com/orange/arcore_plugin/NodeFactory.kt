package com.orange.arcore_plugin

import android.content.Context
import com.google.ar.sceneform.Node
import com.orange.arcore_plugin.models.FlutterArCoreNode
import com.orhanobut.logger.Logger

typealias NodeHandler = (Node?, Throwable?) -> Unit

class NodeFactory {
    companion object {
        fun makeNode(context: Context, flutterNode: FlutterArCoreNode, handler: NodeHandler) {
            Logger.i(flutterNode.toString())
            val node = flutterNode.buildNode()
            RenderableCustomFactory.makeRenderable(context, flutterNode) { renderable, t ->
                if (renderable != null) {
                    node.renderable = renderable
                    handler(node, null)
                } else {
                    handler(null, t)
                }
            }
        }
    }
}