#import "ArcorePlugin.h"
#import <arcore_plugin/arcore_plugin-Swift.h>

@implementation ArcorePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftArcorePlugin registerWithRegistrar:registrar];
}
@end
