import ARCore
import ARKit
import Dispatch
import FirebaseDatabase
import ModelIO
import SceneKit
import ARKit
import SceneKit
import UIKit

class CloudAnchorManager: NSObject, ARSessionDelegate {

    private var _delegate: CloudAnchorManagerDelegate!
    var delegate: CloudAnchorManagerDelegate! {
        get {
            return _delegate
        }
        set(delegate) {
            _delegate = delegate
            gSession.delegate = delegate
        }
    }
    private var sceneView: ARSCNView!
    private var gSession: GARSession!
    private var firebaseReference: DatabaseReference!

    init(scnView: ARSCNView) throws {
        firebaseReference = Database.database().reference()
        sceneView = scnView
        gSession = try GARSession(apiKey: "AIzaSyBJkaUm1mLK_69WC-l8ATf7wJP5N8Lm_Tw",
                bundleIdentifier: nil)
        gSession.delegateQueue = .main
        super.init()
        sceneView.session.delegate = self
    }

    // MARK: - ARSessionDelegate

    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        let garFrame: GARFrame = try! gSession.update(frame)
        delegate.cloudAnchorManager(manager: self, didUpdate: garFrame)
    }

    // MARK: - Public

    func createRoom() {
        firebaseReference.child("last_room_code").runTransactionBlock({ (currentData: MutableData) in
            var roomNumber: NSNumber? = currentData.value as? NSNumber
            if roomNumber == nil || roomNumber?.isEqual(NSNull()) ?? false {
                roomNumber = NSNumber(value: 0)
            }
            var roomNumberInt: Int = roomNumber!.intValue
            roomNumberInt += 1
            let newRoomNumber: NSNumber = NSNumber(value: roomNumberInt )
            let timestampInteger: Int64 = (Int64(NSDate().timeIntervalSince1970 * 1000))
            let timestamp: NSNumber = NSNumber(value: timestampInteger)
            let room: NSDictionary = [
                "display_name": newRoomNumber.stringValue,
                "updated_at_timestamp": timestamp,
            ]
            self.firebaseReference.child("hotspot_list").child(newRoomNumber.stringValue).setValue(room)
            currentData.value = newRoomNumber
            return TransactionResult.success(withValue: currentData)
        }, andCompletionBlock: { (error: Error?, committed: Bool, snapshot: DataSnapshot?) in
            if (error != nil) {
                self.delegate.cloudAnchorManager(manager: self, failedToCreateRoomWithError: error!)
            } else {
                self.delegate.cloudAnchorManager(manager: self,
                        createdRoom: (snapshot?.value as! NSNumber).stringValue)
            }
        })
    }

    func updateRoom(roomCode: String, withAnchor anchor: GARAnchor) {
        firebaseReference.child("hotspot_list").child(roomCode).child("hosted_anchor_id").setValue(anchor.cloudIdentifier)
        let timestampInteger: Int64 = (Int64(NSDate().timeIntervalSince1970 * 1000))
        let timestamp: NSNumber = NSNumber(value: timestampInteger)
        firebaseReference.child("hotspot_list").child(roomCode).child("updated_at_timestamp").setValue(timestamp)
    }

    func resolveAnchorWithRoomCode(roomCode: String, completion: @escaping (GARAnchor) -> Void) {
        firebaseReference.child("hotspot_list").child(roomCode).observe(DataEventType.value,
                with: { (snapshot: DataSnapshot) in
                    var anchorId: String! = nil
                    if (snapshot.value is NSDictionary) {
                        let value: NSDictionary = snapshot.value as! NSDictionary
                        anchorId = value["hosted_anchor_id"] as? String
                    }
                    if (anchorId != nil) {
                        self.firebaseReference.child("hotspot_list").child(roomCode).removeAllObservers()
                        do {
                            try completion(self.gSession.resolveCloudAnchor(withIdentifier: anchorId))
                        } catch {
                        }
                    }
                }
        )
    }

    func stopResolvingAnchorWithRoomCode(roomCode: String) {
        firebaseReference.child("hotspot_list").child(roomCode).removeAllObservers()
    }

    func hostCloudAnchor(arAnchor: ARAnchor) throws -> GARAnchor {
        return try gSession.hostCloudAnchor(arAnchor)
    }

    func removeAnchor(anchor: GARAnchor) {
        gSession.remove(anchor)
    }
}
