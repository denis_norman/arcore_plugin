import ARCore
import ARKit
import Dispatch
import FirebaseDatabase
import ModelIO
import SceneKit
import ARKit
import SceneKit
import UIKit

protocol CloudAnchorManagerDelegate: GARSessionDelegate {

    func cloudAnchorManager(manager: CloudAnchorManager, didUpdate garFrame: GARFrame)

    func cloudAnchorManager(manager: CloudAnchorManager, createdRoom roomCode: String)

    func cloudAnchorManager(manager: CloudAnchorManager, failedToCreateRoomWithError error: Error)

}

