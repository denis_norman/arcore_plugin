import ARCore
import ARKit
import Dispatch
import FirebaseDatabase
import ModelIO
import SceneKit
import ARKit
import SceneKit
import UIKit

enum HelloARState: Int {
    case Default
    case CreatingRoom
    case RoomCreated
    case Hosting
    case HostingFinished
    case EnterRoomCode
    case Resolving
    case ResolvingFinished
}

let kPrivacyNoticeKey: String = "PrivacyNoticeAccepted"
let kPrivacyAlertTitle: String = "Experience it together"
let kPrivacyAlertMessage: String = "To power this session, Google will process visual data from your camera."
let kPrivacyAlertContinueButtonText: String = "Start now"
let kPrivacyAlertLearnMoreButtonText: String = "Learn more"
let kPrivacyAlertBackButtonText: String = "Not now"
let kPrivacyAlertLinkURL: String = "https://developers.google.com/ar/cloud-anchor-privacy"


class ViewController: UIViewController, ARSCNViewDelegate, CloudAnchorManagerDelegate, GARSessionDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var hostButton: UIButton!
    @IBOutlet var resolveButton: UIButton!
    @IBOutlet var roomCodeLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!

    private var resolveTimer: Timer?
    private var arAnchor: ARAnchor?
    private var garAnchor: GARAnchor?
    private var resolvedAnchorNode: SCNNode?
    private var state: HelloARState?
    private var roomCode: String?
    private var message: String?
    private var cloudAnchorManager: CloudAnchorManager?

    // region Overriding UIViewController

    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, sceneView: ARSCNView!, hostButton: UIButton!, resolveButton: UIButton!, roomCodeLabel: UILabel!, messageLabel: UILabel!, resolveTimer: Timer!, arAnchor: ARAnchor!, garAnchor: GARAnchor!, resolvedAnchorNode: SCNNode!, state: HelloARState!, roomCode: String!, message: String!, cloudAnchorManager: CloudAnchorManager!) {
        self.sceneView = sceneView
        self.hostButton = hostButton
        self.resolveButton = resolveButton
        self.roomCodeLabel = roomCodeLabel
        self.messageLabel = messageLabel
        self.resolveTimer = resolveTimer
        self.arAnchor = arAnchor
        self.garAnchor = garAnchor
        self.resolvedAnchorNode = resolvedAnchorNode
        self.state = state
        self.roomCode = roomCode
        self.message = message
        self.cloudAnchorManager = cloudAnchorManager
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    init?(coder aDecoder: NSCoder, sceneView: ARSCNView!, hostButton: UIButton!, resolveButton: UIButton!, roomCodeLabel: UILabel!, messageLabel: UILabel!, resolveTimer: Timer!, arAnchor: ARAnchor!, garAnchor: GARAnchor!, resolvedAnchorNode: SCNNode!, state: HelloARState!, roomCode: String!, message: String!, cloudAnchorManager: CloudAnchorManager!) {
        self.sceneView = sceneView
        self.hostButton = hostButton
        self.resolveButton = resolveButton
        self.roomCodeLabel = roomCodeLabel
        self.messageLabel = messageLabel
        self.resolveTimer = resolveTimer
        self.arAnchor = arAnchor
        self.garAnchor = garAnchor
        self.resolvedAnchorNode = resolvedAnchorNode
        self.state = state
        self.roomCode = roomCode
        self.message = message
        self.cloudAnchorManager = cloudAnchorManager
        super.init(coder: aDecoder)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.numberOfLines = 3
        cloudAnchorManager = try! CloudAnchorManager(scnView: sceneView)
        cloudAnchorManager?.delegate = self
        sceneView.delegate = self
        enterState(state: HelloARState.Default)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if touches.count < 1 || state != HelloARState.RoomCreated {
            return
        }
        let touch: UITouch = touches.first!
        let touchLocation: CGPoint = touch.location(in: sceneView)
        let hitTestResults: [ARHitTestResult] = sceneView.hitTest(touchLocation,
                types: ARHitTestResult.ResultType(rawValue: ARHitTestResult.ResultType.existingPlaneUsingExtent.rawValue |
                        ARHitTestResult.ResultType.estimatedHorizontalPlane.rawValue))

        if hitTestResults.count > 0 {
            let result: ARHitTestResult = hitTestResults.first!
            addAnchorWithTransform(transform: result.worldTransform)
        }
    }

    // endregion

    // region Anchor Hosting / Resolving

    func resolveAnchorWithRoomCode(roomCode: String) {
        self.roomCode = roomCode
        enterState(state: HelloARState.Resolving)
        cloudAnchorManager?.resolveAnchorWithRoomCode(roomCode: roomCode, completion: { (anchor: GARAnchor) in
            self.garAnchor = anchor
        })
    }

    func addAnchorWithTransform(transform: matrix_float4x4) {
        arAnchor = ARAnchor(transform: transform)
        sceneView.session.add(anchor: arAnchor!)
        garAnchor = try! cloudAnchorManager?.hostCloudAnchor(arAnchor: arAnchor!)
        enterState(state: HelloARState.Hosting)
    }

    // endregion

    // region Actions

    @IBAction func hostButtonPressed() {
        if state == HelloARState.Default {
            checkPrivacyNotice(completion: { (shouldContinue: Bool) in
                if shouldContinue {
                    self.enterState(state: HelloARState.CreatingRoom)
                    self.cloudAnchorManager?.createRoom()
                }
            })
        } else {
            enterState(state: HelloARState.Default)
        }
    }

    @IBAction func resolveButtonPressed() {
        if state == HelloARState.Default {
            checkPrivacyNotice(completion: { (shouldContinue: Bool) in
                if shouldContinue {
                    self.enterState(state: HelloARState.EnterRoomCode)
                }
            })
        } else {
            enterState(state: HelloARState.Default)
        }
    }

    // endregion

    // region CloudAnchorManagerDelegate

    func cloudAnchorManager(manager: CloudAnchorManager, createdRoom roomCode: String) {
        self.roomCode = roomCode
        enterState(state: HelloARState.RoomCreated)
    }

    func cloudAnchorManager(manager: CloudAnchorManager, failedToCreateRoomWithError error: Error) {
        enterState(state: HelloARState.Default)
    }

    func cloudAnchorManager(manager: CloudAnchorManager, didUpdate garFrame: GARFrame) {
        for garAnchor in garFrame.updatedAnchors {
            if garAnchor.isEqual(garAnchor) {
                resolvedAnchorNode?.simdTransform = garAnchor.transform
                resolvedAnchorNode?.isHidden = garAnchor.hasValidTransform
            }
        }
    }

    // endregion

    // region GARSessionDelegate

    func session(_ session: GARSession, didHostAnchor anchor: GARAnchor) {
        if state != HelloARState.Hosting || anchor != garAnchor {
            return
        }
        garAnchor = anchor
        if roomCode != nil {
            cloudAnchorManager?.updateRoom(roomCode: roomCode!, withAnchor: anchor)
            enterState(state: HelloARState.HostingFinished)
        }
    }

    func session(_ session: GARSession, didFailToHostAnchor anchor: GARAnchor) {
        if state != HelloARState.Hosting || anchor != garAnchor {
            return
        }
        garAnchor = anchor
        enterState(state: HelloARState.HostingFinished)
    }

    func session(_ session: GARSession, didResolve anchor: GARAnchor) {
        let seconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            if self.state != HelloARState.Resolving || anchor != self.garAnchor {
                return
            }
            self.garAnchor = anchor
            self.resolvedAnchorNode = self.andyNode()
            self.resolvedAnchorNode?.simdTransform = anchor.transform
            self.sceneView.scene.rootNode.addChildNode(self.resolvedAnchorNode!)
            self.enterState(state: HelloARState.ResolvingFinished)
        }
    }

    func session(_ session: GARSession, didFailToResolve anchor: GARAnchor) {
        if state != HelloARState.Resolving || anchor != garAnchor {
            return
        }
        garAnchor = anchor
        enterState(state: HelloARState.ResolvingFinished)
    }

    // endregion

    // region Helper Methods

    func privacyNoticeAccepted() -> Bool {
        return UserDefaults.standard.bool(forKey: kPrivacyNoticeKey)
    }

    func setPrivacyNoticeAccepted(accepted: Bool) {
        UserDefaults.standard.set(accepted, forKey: kPrivacyNoticeKey)
    }

    func runSession() {
        let configuration: ARWorldTrackingConfiguration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = ARConfiguration.WorldAlignment.gravity
        configuration.planeDetection = ARWorldTrackingConfiguration.PlaneDetection.horizontal

        sceneView.session.run(configuration)
    }

    func checkPrivacyNotice(completion: @escaping (Bool) -> Void) {
        let innerCompletion: (Bool) -> Void = { (shouldContinue: Bool) in
            if shouldContinue {
                self.privacyNoticeAccepted()
                self.runSession()
            }
            completion(shouldContinue)
        }

        if privacyNoticeAccepted() {
            innerCompletion(true)
            return
        }

        let alertController: UIAlertController = UIAlertController(title: kPrivacyAlertTitle,
                message: kPrivacyAlertMessage,
                preferredStyle: UIAlertController.Style.alert)
        let okAction: UIAlertAction = UIAlertAction(title: kPrivacyAlertContinueButtonText,
                style: UIAlertAction.Style.default,
                handler: { (action: UIAlertAction) in
                    innerCompletion(true)
                })
        let learnMoreAction: UIAlertAction = UIAlertAction(title: kPrivacyAlertLearnMoreButtonText,
                style: UIAlertAction.Style.default,
                handler: { (action: UIAlertAction) in
                    UIApplication.shared.open(NSURL(string: kPrivacyAlertLinkURL)! as URL,
                            options: [:],
                            completionHandler: nil)
                    innerCompletion(false)
                })
        let cancelAction: UIAlertAction = UIAlertAction(title: kPrivacyAlertBackButtonText,
                style: UIAlertAction.Style.default,
                handler: { (action: UIAlertAction) in
                    innerCompletion(false)
                })
        alertController.addAction(okAction)
        alertController.addAction(learnMoreAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: false, completion: {})
    }

    func resolveTimerFired() {
        if state == HelloARState.Resolving {
            message = """
                      Still resolving the anchor. Please make sure you're looking at where the
                      Cloud Anchor was hosted. Or, try to re-join the room.
                      """
            updateMessageLabel()
        }
    }

    func updateMessageLabel() {
        messageLabel.text = message
        if roomCode != nil {
            roomCodeLabel.text = String(format: "Room: %@", roomCode!)
        }
    }

    func toggleButton(button: UIButton, enabled: Bool, title: String) {
        button.isEnabled = enabled
        button.setTitle(title, for: UIControl.State.normal)
    }

    func cloudStateString(cloudState: GARCloudAnchorState) -> String {
        switch (cloudState) {
        case GARCloudAnchorState.none:
            return "None"
        case GARCloudAnchorState.success:
            return "Success"
        case GARCloudAnchorState.errorInternal:
            return "ErrorInternal"
        case GARCloudAnchorState.taskInProgress:
            return "TaskInProgress"
        case GARCloudAnchorState.errorNotAuthorized:
            return "ErrorNotAuthorized"
        case GARCloudAnchorState.errorResourceExhausted:
            return "ErrorResourceExhausted"
        case GARCloudAnchorState.errorHostingDatasetProcessingFailed:
            return "ErrorHostingDatasetProcessingFailed"
        case GARCloudAnchorState.errorCloudIdNotFound:
            return "ErrorCloudIdNotFound"
        case GARCloudAnchorState.errorResolvingSdkVersionTooNew:
            return "ErrorResolvingSdkVersionTooNew"
        case GARCloudAnchorState.errorResolvingSdkVersionTooOld:
            return "ErrorResolvingSdkVersionTooOld"
        case GARCloudAnchorState.errorServiceUnavailable:
            return "ErrorServiceUnavailable"
        default:
            return "Unknown"
        }
    }

    func showRoomCodeDialog() {
        let alertController: UIAlertController = UIAlertController(title: "ENTER ROOM CODE",
                message: "",
                preferredStyle: UIAlertController.Style.alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK",
                style: UIAlertAction.Style.default,
                handler: { (action: UIAlertAction) in
                    let roomCode: String = alertController.textFields![0].text!
                    if roomCode.count == 0 {
                        self.enterState(state: HelloARState.Default)
                    } else {
                        self.resolveAnchorWithRoomCode(roomCode: roomCode)
                    }
                })
        let cancelAction: UIAlertAction = UIAlertAction(title: "CANCEL",
                style: UIAlertAction.Style.default,
                handler: { (action: UIAlertAction) in
                    self.enterState(state: HelloARState.Default)
                })
        alertController.addTextField(configurationHandler: { (textField: UITextField) in
            textField.keyboardType = UIKeyboardType.numberPad
        })
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: false, completion: {})
    }

    func enterState(state: HelloARState) {
        switch (state) {
        case HelloARState.Default:
            if arAnchor != nil {
                sceneView.session.remove(anchor: arAnchor!)
                arAnchor = nil
            }
            if resolvedAnchorNode != nil {
                resolvedAnchorNode?.removeFromParentNode()
                resolvedAnchorNode = nil
            }
            if garAnchor != nil {
                cloudAnchorManager?.removeAnchor(anchor: garAnchor!)
                garAnchor = nil
            }
            if state == HelloARState.CreatingRoom {
                message = "Failed to create room. Tap HOST or RESOLVE to begin."
            } else {
                message = "Tap HOST or RESOLVE to begin."
            }
            if state == HelloARState.EnterRoomCode {
                dismiss(animated: false, completion: {})
            } else if state == HelloARState.Resolving {
                if roomCode != nil {
                    cloudAnchorManager?.stopResolvingAnchorWithRoomCode(roomCode: roomCode!)
                    if resolveTimer != nil {
                        resolveTimer?.invalidate()
                        resolveTimer = nil
                    }
                }
            }
            toggleButton(button: hostButton, enabled: true, title: "HOST")
            toggleButton(button: resolveButton, enabled: true, title: "RESOLVE")
            roomCode = ""
            break
        case HelloARState.CreatingRoom:
            message = "Creating room..."
            toggleButton(button: hostButton, enabled: false, title: "HOST")
            toggleButton(button: resolveButton, enabled: false, title: "RESOLVE")
            break
        case HelloARState.RoomCreated:
            message = "Tap on a plane to create anchor and host."
            toggleButton(button: hostButton, enabled: true, title: "CANCEL")
            toggleButton(button: resolveButton, enabled: false, title: "RESOLVE")
            break
        case HelloARState.Hosting:
            message = "Hosting anchor..."
            break
        case HelloARState.HostingFinished:
            if garAnchor?.cloudState != nil {
                message =
                        String(format: "Finished hosting: %@",
                                cloudStateString(cloudState: garAnchor!.cloudState))
            }
            break
        case HelloARState.EnterRoomCode:
            showRoomCodeDialog()
            break
        case HelloARState.Resolving:
            dismiss(animated: false, completion: {})
            message = "Resolving anchor..."
            toggleButton(button: hostButton, enabled: false, title: "HOST")
            toggleButton(button: resolveButton, enabled: true, title: "CANCEL")
            resolveTimer = Timer.scheduledTimer(withTimeInterval: 10,
                    repeats: false,
                    block: { (timer: Timer) in
                        self.resolveTimerFired()
                    })
            break
        case HelloARState.ResolvingFinished:
            if resolveTimer != nil {
                resolveTimer?.invalidate()
                resolveTimer = nil
            }
            if garAnchor?.cloudState != nil {
                message =
                        String(format: "Finished resolving: %@",
                                cloudStateString(cloudState: garAnchor!.cloudState))
            }
            break
        }
        self.state = state
        updateMessageLabel()
    }

    // endregion

    // region Helper method to generate an SCNNode with Android geometry.

    func andyNode() -> SCNNode {
        let scene: SCNScene = SCNScene(named: "Andy.scnassets/Andy.scn")!
        let node = scene.rootNode.childNode(withName: "Andy", recursively: false)
        return node!
    }

    // endregion

    // region ARSCNViewDelegate

    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        if (anchor is ARPlaneAnchor) == false {
            return andyNode()
        } else {
            return SCNNode()
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if (anchor is ARPlaneAnchor) {
            let planeAnchor: ARPlaneAnchor = anchor as! ARPlaneAnchor

            let width: CGFloat = CGFloat(planeAnchor.extent.x)
            let height: CGFloat = CGFloat(planeAnchor.extent.z)
            let plane: SCNPlane = SCNPlane(width: width, height: height)

            plane.materials.first!.diffuse.contents =
                    UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 0.3)
            let planeNode: SCNNode = SCNNode(geometry: plane)
            let x: CGFloat = CGFloat(planeAnchor.center.x)
            let y: CGFloat = CGFloat(planeAnchor.center.y)
            let z: CGFloat = CGFloat(planeAnchor.center.z)
            planeNode.position = SCNVector3Make(Float(x), Float(y), Float(z))
            planeNode.eulerAngles = SCNVector3Make(Float(-M_PI / 2), 0, 0)
            node.addChildNode(planeNode)
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if (anchor is ARPlaneAnchor) {
            let planeAnchor: ARPlaneAnchor = anchor as! ARPlaneAnchor
            let planeNode: SCNNode = node.childNodes.first!
            let plane: SCNPlane = planeNode.geometry as! SCNPlane
            let width: CGFloat = CGFloat(planeAnchor.extent.x)
            let height: CGFloat = CGFloat(planeAnchor.extent.z)
            plane.width = width
            plane.height = height
            let x: CGFloat = CGFloat(planeAnchor.center.x)
            let y: CGFloat = CGFloat(planeAnchor.center.y)
            let z: CGFloat = CGFloat(planeAnchor.center.z)
            planeNode.position = SCNVector3Make(Float(x), Float(y), Float(z))
        }
    }

    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        if (anchor is ARPlaneAnchor) {
            let planeNode: SCNNode = node.childNodes.first!
            planeNode.removeFromParentNode()
        }
    }

    // endregion
}
