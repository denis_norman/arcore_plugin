import SceneKit

class DecodableUtils: NSObject {
    static func parseVector3(_ vector: [AnyHashable: Any]?) -> SCNVector3 {
        let x = (vector?["x"] as? NSNumber)?.floatValue ?? 0.0
        let y = (vector?["y"] as? NSNumber)?.floatValue ?? 0.0
        let z = (vector?["z"] as? NSNumber)?.floatValue ?? 0.0
        return SCNVector3Make(x, y, z)
    }

    static func parseVector4(_ vector: [AnyHashable: Any]?) -> SCNVector4 {
        let x = (vector?["x"] as? NSNumber)?.floatValue ?? 0.0
        let y = (vector?["y"] as? NSNumber)?.floatValue ?? 0.0
        let z = (vector?["z"] as? NSNumber)?.floatValue ?? 0.0
        let w = (vector?["w"] as? NSNumber)?.floatValue ?? 0.0
        return SCNVector4Make(x, y, z, w)
    }
}
