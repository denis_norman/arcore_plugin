import ARKit;

class CodableUtils: NSObject {

     class func convertSimdFloat3(toString vector: simd_float3) -> String {
        return "\(vector[0]) \(vector[1]) \(vector[2])"
    }

     class func convertSimdFloat4x4(toString matrix: simd_float4x4) -> String {
        var ret = ""
        for i in 0..<4 {
            for j in 0..<4 {
                ret += "\(matrix[i][j]) "
            }
        }
        return ret
    }

    class func convertARAnchor(toDictionary anchor: ARAnchor) -> [AnyHashable: Any] {
        var params = [
            "identifier": anchor.identifier.uuidString,
            "transform": CodableUtils.convertSimdFloat4x4(toString: anchor.transform)
        ]
        if type(of: anchor) === ARPlaneAnchor.self {
            let plane = anchor as? ARPlaneAnchor
            params["anchorType"] = "planeAnchor"
            if let center = plane?.center {
                params["center"] = CodableUtils.convertSimdFloat3(toString: center)
            }
            if let extent = plane?.extent {
                params["extent"] = CodableUtils.convertSimdFloat3(toString: extent)
            }
        }
        if type(of: anchor) === ARImageAnchor.self {
            let image = anchor as? ARImageAnchor
            params["anchorType"] = "imageAnchor"
            params["referenceImageName"] = image?.referenceImage.name
        }
        if type(of: anchor) === ARFaceAnchor.self {
            params["anchorType"] = "faceAnchor"
            let faceAnchor = anchor as? ARFaceAnchor
            if let leftEyeTransform = faceAnchor?.leftEyeTransform {
                params["leftEyeTransform"] = CodableUtils.convertSimdFloat4x4(toString: leftEyeTransform)
            }
            if let rightEyeTransform = faceAnchor?.rightEyeTransform {
                params["rightEyeTransform"] = CodableUtils.convertSimdFloat4x4(toString: rightEyeTransform)
            }
            params["blendShapes"] = "\(faceAnchor!.blendShapes)"
        }
        return params
    }
}
