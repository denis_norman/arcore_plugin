import SceneKit
import ARKit

class GeometryBuilder: NSObject {

    static func createGeometry(_ arguments: [AnyHashable: Any]?, withDevice device: NSObject?) -> SCNGeometry? {
        guard let arguments = arguments else {
            return nil
        }
        guard let device = device else {
            return nil
        }
        var selector: Selector?
        guard let type = arguments["dartType"] as? String else {
            return nil
        }
        switch type {
        case "ARKitSphere":
            selector = #selector(getSphere)
            break
        case "ARKitPlane":
            selector = #selector(getPlane)
            break
        case "ARKitText":
            selector = #selector(getText)
            break
        case "ARKitBox":
            selector = #selector(getBox)
            break
        case "ARKitLine":
            selector = #selector(getLine)
            break
        case "ARKitCylinder":
            selector = #selector(getCylinder)
            break
        case "ARKitCone":
            selector = #selector(getCone)
            break
        case "ARKitPyramid":
            selector = #selector(getPyramid)
            break
        case "ARKitTube":
            selector = #selector(getTube)
            break
        case "ARKitTorus":
            selector = #selector(getTorus)
            break
        case "ARKitCapsule":
            selector = #selector(getCapsule)
            break
        case "ARKitFace":
            selector = #selector(getFace)
            break
        default:
            selector = nil
            break
        }
        if selector == nil {
            return nil
        }
        guard let imp = method(for: selector) else {
            return nil
        }
        guard let myfunc = imp as? (Any?, Selector?, [AnyHashable: Any]?, Any?) -> SCNGeometry? else {
            return nil
        }
        guard let geometry = myfunc(self, selector, arguments, device) else {
            return nil
        }
        guard let materialsString = arguments["materials"] as? [Any] else {
            return nil
        }
        if let materials = getMaterials(materialsString) {
            geometry.materials = materials
        }
        return geometry
    }

    static func getMaterials(_ materialsString: [Any]?) -> [SCNMaterial]? {
        guard let materialsString = materialsString else {
            return nil
        }
        if materialsString.count == 0 {
            return nil
        }
        var materials: [SCNMaterial] = []
        for material in materialsString {
            guard let material = material as? [AnyHashable: Any] else {
                continue
            }
            if let scnmaterial = getMaterial(material) {
                materials.append(scnmaterial)
            }
        }
        return materials
    }


    static func getMaterial(_ materialString: [AnyHashable: Any]) -> SCNMaterial? {
        let material = SCNMaterial()
        for property in [
            "diffuse",
            "ambient",
            "specular",
            "emission",
            "transparent",
            "reflective",
            "multiply",
            "normal",
            "displacement",
            "ambientOcclusion",
            "selfIllumination",
            "metalness",
            "roughness"
        ] {
            applyMaterialProperty(propertyName: property, withPropertyDictionary: materialString, and: material)
        }
        if let shininess = (materialString["shininess"] as? NSNumber)?.doubleValue {
            material.shininess = CGFloat(shininess)
        }
        if let transparency = (materialString["transparency"] as? NSNumber)?.doubleValue {
            material.transparency = CGFloat(transparency)
        }
        if let lightingModelName = (materialString["lightingModelName"] as? NSNumber)?.intValue {
            material.lightingModel = getLightingMode(lightingModelName)
        }
        if let integer = (materialString["fillMode"] as? NSNumber)?.intValue {
            if let fillMode = SCNFillMode(rawValue: UInt(integer)) {
                material.fillMode = fillMode
            }
        }
        if let integer = (materialString["cullMode"] as? NSNumber)?.intValue {
            if let cullMode = SCNCullMode(rawValue: integer) {
                material.cullMode = cullMode
            }
        }
        if let integer = (materialString["transparencyMode"] as? NSNumber)?.intValue {
            if let transparencyMode = SCNTransparencyMode(rawValue: integer) {
                material.transparencyMode = transparencyMode
            }
        }
        if let locksAmbientWithDiffuse = (materialString["locksAmbientWithDiffuse"] as? NSNumber)?.boolValue {
            material.locksAmbientWithDiffuse = locksAmbientWithDiffuse
        }
        if let writesToDepthBuffer = (materialString["writesToDepthBuffer"] as? NSNumber)?.boolValue {
            material.writesToDepthBuffer = writesToDepthBuffer
        }
        if let colorBufferWriteMask = (materialString["colorBufferWriteMask"] as? NSNumber)?.intValue {
            material.colorBufferWriteMask = getColorMask(colorBufferWriteMask)
        }
        if let integer = (materialString["blendMode"] as? NSNumber)?.intValue {
            if let blendMode = SCNBlendMode(rawValue: integer) {
                material.blendMode = blendMode
            }
        }
        if let isDoubleSided = (materialString["doubleSided"] as? NSNumber)?.boolValue {
            material.isDoubleSided = isDoubleSided
        }
        return material
    }


    static func applyMaterialProperty(propertyName: String, withPropertyDictionary dict: [AnyHashable: Any]?, and material: SCNMaterial) {
        if dict?[propertyName] != nil {
            let property: SCNMaterialProperty! = material.value(forKey: propertyName) as? SCNMaterialProperty
            property.contents = getMaterialProperty(propertyString: (dict?[propertyName] as? NSDictionary)! as? [AnyHashable: Any])
        }
    }

    static func getMaterialProperty(propertyString: [AnyHashable: Any]?) -> Any! {
        if propertyString?["image"] != nil {
            var img: UIImage! = UIImage(named: (propertyString?["image"] as? String) ?? "")
            if img == nil {
                let asset_path = (propertyString?["image"] as? String) ?? ""
                let path = Bundle.main.path(forResource: CloudAnchor.registrar.lookupKey(forAsset: asset_path), ofType: nil) ?? ""
                img = UIImage(named: path)
            }
            return img
        } else if propertyString?["color"] != nil {
            let color = (propertyString?["color"] as? NSNumber)?.intValue ?? 0
            return UIColor.fromRGB(color)
        } else if propertyString?["url"] != nil {
            return propertyString?["url"]
        }
        return nil
    }

    static func getLightingMode(_ mode: Int) -> SCNMaterial.LightingModel {
        switch mode {
        case 0:
            return .phong
        case 1:
            return .blinn
        case 2:
            return .lambert
        case 3:
            return .constant
        default:
            return .physicallyBased
        }
    }

    static func getColorMask(_ mode: Int) -> SCNColorMask {
        switch mode {
        case 0:
            return []
        case 1:
            return .red
        case 2:
            return .green
        case 3:
            return .blue
        case 4:
            return .alpha
        default:
            return .all
        }
    }


    @objc private func getSphere(_ geometryArguments: [AnyHashable: Any]?) -> SCNSphere! {
        let radius = (geometryArguments?["radius"] as? NSNumber)?.floatValue ?? 0.0
        return SCNSphere(radius: CGFloat(radius))
    }

    @objc private func getPlane(_ geometryArguments: [AnyHashable: Any]?) -> SCNPlane? {
        let width = (geometryArguments?["width"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        let widthSegmentCount = (geometryArguments?["widthSegmentCount"] as? NSNumber)?.intValue ?? 0
        let heightSegmentCount = (geometryArguments?["heightSegmentCount"] as? NSNumber)?.intValue ?? 0
        let plane = SCNPlane(width: CGFloat(width), height: CGFloat(height))
        plane.widthSegmentCount = widthSegmentCount
        plane.heightSegmentCount = heightSegmentCount
        return plane
    }


    @objc private func getText(_ geometryArguments: [AnyHashable: Any]?) -> SCNText! {
        let extrusionDepth = (geometryArguments?["extrusionDepth"] as? NSNumber)?.floatValue ?? 0.0
        return SCNText(string: geometryArguments?["text"], extrusionDepth: CGFloat(extrusionDepth))
    }

    @objc private func getBox(_ geometryArguments: [AnyHashable: Any]?) -> SCNBox! {
        let width = (geometryArguments?["width"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        let length = (geometryArguments?["length"] as? NSNumber)?.floatValue ?? 0.0
        let chamferRadius = (geometryArguments?["chamferRadius"] as? NSNumber)?.floatValue ?? 0.0
        return SCNBox(width: CGFloat(width), height: CGFloat(height), length: CGFloat(length), chamferRadius: CGFloat(chamferRadius))
    }

    @objc func getLine(_ geometryArguments: [AnyHashable: Any]?) -> SCNGeometry? {
        let fromVector = DecodableUtils.parseVector3(geometryArguments?["fromVector"] as? [AnyHashable: Any])
        let toVector = DecodableUtils.parseVector3(geometryArguments?["toVector"] as? [AnyHashable: Any])
        let vertices = [fromVector, toVector]
        let source = SCNGeometrySource(vertices: vertices)
        let indexes: [Int] = [0, 1]
        let dataIndexes = Data(bytes: indexes, count: MemoryLayout<[Int]>.size)
        let element = SCNGeometryElement(data: dataIndexes, primitiveType: .line, primitiveCount: 1, bytesPerIndex: MemoryLayout<Int>.size)
        return SCNGeometry(sources: [source], elements: [element])
    }

    @objc private func getCylinder(_ geometryArguments: [AnyHashable: Any]?) -> SCNCylinder! {
        let radius = (geometryArguments?["radius"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        return SCNCylinder(radius: CGFloat(radius), height: CGFloat(height))
    }

    @objc private func getCone(_ geometryArguments: [AnyHashable: Any]?) -> SCNCone! {
        let topRadius = (geometryArguments?["topRadius"] as? NSNumber)?.floatValue ?? 0.0
        let bottomRadius = (geometryArguments?["bottomRadius"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        return SCNCone(topRadius: CGFloat(topRadius), bottomRadius: CGFloat(bottomRadius), height: CGFloat(height))
    }

    @objc private func getPyramid(_ geometryArguments: [AnyHashable: Any]?) -> SCNPyramid! {
        let width = (geometryArguments?["width"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        let length = (geometryArguments?["length"] as? NSNumber)?.floatValue ?? 0.0
        return SCNPyramid(width: CGFloat(width), height: CGFloat(height), length: CGFloat(length))
    }

    @objc private func getTube(_ geometryArguments: [AnyHashable: Any]?) -> SCNTube! {
        let innerRadius = (geometryArguments?["innerRadius"] as? NSNumber)?.floatValue ?? 0.0
        let outerRadius = (geometryArguments?["outerRadius"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        return SCNTube(innerRadius: CGFloat(innerRadius), outerRadius: CGFloat(outerRadius), height: CGFloat(height))
    }

    @objc private func getTorus(_ geometryArguments: [AnyHashable: Any]?) -> SCNTorus! {
        let ringRadius = (geometryArguments?["ringRadius"] as? NSNumber)?.floatValue ?? 0.0
        let pipeRadius = (geometryArguments?["pipeRadius"] as? NSNumber)?.floatValue ?? 0.0
        return SCNTorus(ringRadius: CGFloat(ringRadius), pipeRadius: CGFloat(pipeRadius))
    }

    @objc private func getCapsule(_ geometryArguments: [AnyHashable: Any]?) -> SCNCapsule! {
        let capRadius = (geometryArguments?["capRadius"] as? NSNumber)?.floatValue ?? 0.0
        let height = (geometryArguments?["height"] as? NSNumber)?.floatValue ?? 0.0
        return SCNCapsule(capRadius: CGFloat(capRadius), height: CGFloat(height))
    }

    @objc private func getFace(_ geometryArguments: [AnyHashable: Any]?, withDeivce device: Any?) -> ARSCNFaceGeometry? {
        if let device = device as? MTLDevice {
            return ARSCNFaceGeometry(device: device)
        }
        return nil
    }
}
