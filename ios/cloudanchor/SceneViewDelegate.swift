import ARKit
import Flutter

class SceneViewDelegate: NSObject, ARSCNViewDelegate {
    
    private var channel: FlutterMethodChannel!
    
    init(channel: FlutterMethodChannel!) {
        super.init()
        self.channel = channel
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        channel.invokeMethod("onError", arguments: error.localizedDescription)
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        channel.invokeMethod("onSessionWasInterrupted", arguments: nil)
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        channel.invokeMethod("onSessionInterruptionEnded", arguments: nil)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if node.name == nil {
            node.name = NSUUID().uuidString
        }
        let params = prepareParams(forAnchorEventwithNode: node, andAnchor: anchor)
        channel.invokeMethod("didAddNodeForAnchor", arguments: params)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        let params = prepareParams(forAnchorEventwithNode: node, andAnchor: anchor)
        channel.invokeMethod("didUpdateNodeForAnchor", arguments: params)
    }
    
    func prepareParams(forAnchorEventwithNode node: SCNNode?, andAnchor anchor: ARAnchor?) -> [String : String]? {
        guard let node = node else {
            return nil
        }
        guard let anchor = anchor else {
            return nil
        }
        var params = [
            "node_name": node.name ?? ""
        ]
        for (k, v) in CodableUtils.convertARAnchor(toDictionary: anchor) {
            params[k as? String ?? ""] = v as? String
        }
        return params
    }
}
