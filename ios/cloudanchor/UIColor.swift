import UIKit

extension UIColor {
    class func fromRGB(_ rgbValue: Int) -> UIColor? {
        return UIColor(red: CGFloat((Float((rgbValue & 0xff0000) >> 16)) / 255.0),
                       green: CGFloat((Float((rgbValue & 0xff00) >> 8)) / 255.0),
                       blue: CGFloat((Float(rgbValue & 0xff)) / 255.0),
                       alpha: CGFloat((Float((rgbValue & 0xff000000) >> 24)) / 255.0))
    }
}
