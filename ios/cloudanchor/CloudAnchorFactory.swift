import Flutter
import ARKit
import SceneKit

class CloudAnchorFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger!
    
    init(messenger: FlutterBinaryMessenger) {
        super.init()
        self.messenger = messenger
    }
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        let arkitController = CloudAnchorController(frame: frame,
                                                    viewIdentifier: viewId,
                                                    arguments: args,
                                                    binaryMessenger: messenger)
        return arkitController
    }
    
    private func createArgsCodec() -> FlutterStandardMessageCodec {
        FlutterStandardMessageCodec.sharedInstance()
    }
}


class CloudAnchorController: NSObject, FlutterPlatformView {
    
    private var sceneView: ARSCNView!
    private var configuration: ARConfiguration!
    private var planeDetection: ARWorldTrackingConfiguration.PlaneDetection!
    private var viewId: Int64!
    private var channel: FlutterMethodChannel!
    private var delegate: SceneViewDelegate!
    private var forceUserTapOnCenter: Bool!
    
    init(frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?, binaryMessenger messenger: FlutterBinaryMessenger) {
        super.init()
        self.viewId = viewId
        sceneView = ARSCNView(frame: frame)
        let channelName: String! = String(format: "arkit_%lld", viewId)
        channel = FlutterMethodChannel.init(name: channelName, binaryMessenger: messenger)
        channel.setMethodCallHandler({ (call: FlutterMethodCall!, result: FlutterResult) in
            self.onMethodCall(call: call, result: result)
        })
        delegate = SceneViewDelegate(channel: channel)
        sceneView.delegate = delegate
    }
    
    func view() -> UIView {
        return sceneView
    }
    
    func onMethodCall(call: FlutterMethodCall!, result: FlutterResult) {
        switch call.method {
        case "init":
            initCloudAnchor(call, result: result)
        case "addARKitNode":
            onAddNode(call, result: result)
        case "removeARKitNode":
            onRemoveNode(call, result: result)
        case "getNodeBoundingBox":
            onGetNodeBoundingBox(call, result: result)
        case "positionChanged":
            updatePosition(call, andResult: result)
        case "rotationChanged":
            updateRotation(call, andResult: result)
        case "eulerAnglesChanged":
            updateEulerAngles(call, andResult: result)
        case "scaleChanged":
            updateScale(call, andResult: result)
        case "updateSingleProperty":
            updateSingleProperty(call, andResult: result)
        case "updateMaterials":
            updateMaterials(call, andResult: result)
        case "updateFaceGeometry":
            updateFaceGeometry(call, andResult: result)
        case "getLightEstimate":
            onGetLightEstimate(call, andResult: result)
        case "projectPoint":
            onProjectPoint(call, andResult: result)
        case "playAnimation":
            onPlayAnimation(call, andResult: result)
        case "stopAnimation":
            onStopAnimation(call, andResult: result)
        case "dispose":
            sceneView.session.pause()
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func initCloudAnchor(_ call: FlutterMethodCall?, result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        if let showStatistics = (arguments["showStatistics"] as? NSNumber)?.boolValue {
            sceneView.showsStatistics = showStatistics
        }
        if let autoenablesDefaultLighting = (arguments["autoenablesDefaultLighting"] as? NSNumber)?.boolValue {
            sceneView.autoenablesDefaultLighting = autoenablesDefaultLighting
        }
        if let forceUserTapOnCenter = (arguments["forceUserTapOnCenter"] as? NSNumber)?.boolValue {
            self.forceUserTapOnCenter = forceUserTapOnCenter
        }
        if let requestedPlaneDetection = (arguments["planeDetection"] as? NSNumber)?.intValue {
            planeDetection = getPlane(with: requestedPlaneDetection)
        }
        if (arguments["enableTapRecognizer"] as? NSNumber)?.boolValue ?? false {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            sceneView.addGestureRecognizer(tapGestureRecognizer)
        }
        if (arguments["enablePinchRecognizer"] as? NSNumber)?.boolValue ?? false {
            let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
            sceneView.addGestureRecognizer(pinchGestureRecognizer)
        }
        if (arguments["enablePanRecognizer"] as? NSNumber)?.boolValue ?? false {
            let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
            sceneView.addGestureRecognizer(panGestureRecognizer)
        }
        sceneView.debugOptions = getDebugOptions(arguments)
        configuration = buildConfiguration(params: arguments)
        sceneView.session.run(configuration)
        result(nil)
    }
    
    private func buildConfiguration(params: [AnyHashable: Any]?) -> ARConfiguration! {
        let configurationType = (params?["configuration"] as? NSNumber)?.intValue ?? -1
        var configuration: ARConfiguration!
        if configurationType == 0 {
            if ARWorldTrackingConfiguration.isSupported {
                let worldTrackingConfiguration: ARWorldTrackingConfiguration! = ARWorldTrackingConfiguration()
                worldTrackingConfiguration.planeDetection = planeDetection
                let detectionImages = params?["detectionImagesGroupName"] as? String ?? ""
                worldTrackingConfiguration.detectionImages = ARReferenceImage.referenceImages(inGroupNamed: detectionImages as String, bundle: nil)
                configuration = worldTrackingConfiguration
            }
        } else if configurationType == 1 {
            if ARFaceTrackingConfiguration.isSupported {
                let faceTrackingConfiguration: ARFaceTrackingConfiguration! = ARFaceTrackingConfiguration()
                configuration = faceTrackingConfiguration
            }
        }
        let worldAlignment = (params?["worldAlignment"] as? NSNumber)?.intValue ?? -1
        configuration.worldAlignment = getWorldAlignment(from: worldAlignment)
        return configuration
    }
    
    private func onAddNode(_ call: FlutterMethodCall, result: FlutterResult) {
        let arguments = call.arguments as? [AnyHashable: Any]
        let geometryArguments = arguments?["geometry"] as? [AnyHashable: Any]
        let geometry: SCNGeometry! = GeometryBuilder.createGeometry(geometryArguments, withDevice: sceneView?.device as? NSObject)
        addNodeToScene(with: geometry, andCall: call, andResult: result)
    }
    
    private func onRemoveNode(_ call: FlutterMethodCall, result: FlutterResult) {
        let arguments = call.arguments as? [AnyHashable: Any]
        let nodeName = arguments?["nodeName"] as? String ?? ""
        let node: SCNNode! = sceneView.scene.rootNode.childNode(withName: nodeName, recursively: true)
        node.removeFromParentNode()
        result(nil)
    }
    
    private func onGetNodeBoundingBox(_ call: FlutterMethodCall?, result: FlutterResult) {
        if let arguments = call?.arguments as? [AnyHashable: Any] {
            if let geometryArguments = arguments["geometry"] as? [AnyHashable : Any] {
                if let geometry = GeometryBuilder.createGeometry(geometryArguments, withDevice: sceneView?.device as? NSObject) {
                    if let node = getNode(with: geometry, fromDict: arguments) {
                        let minVector: SCNVector3 = node.boundingBox.min
                        let maxVector: SCNVector3 = node.boundingBox.max
                        result([
                            CodableUtils.convertSimdFloat3(toString: SIMD3<Float>.init(minVector)),
                            CodableUtils.convertSimdFloat3(toString: SIMD3<Float>.init(maxVector))
                        ])
                    }
                }
            }
        }
    }
    
    @objc private func handleTap(from recognizer: UITapGestureRecognizer?) {
        guard let recognizer = recognizer else {
            return
        }
        guard let sceneView = recognizer.view as? ARSCNView else {
            return
        }
        let touchLocation = self.forceUserTapOnCenter
            ? self.sceneView.center
            : recognizer.location(in: sceneView)
        let hitResults = sceneView.hitTest(touchLocation, options: [:])
        if hitResults.count != 0 {
            guard let node = hitResults.first?.node else {
                return
            }
            channel.invokeMethod("onNodeTap", arguments: node.name)
        }
        let arHitResults = sceneView.hitTest(touchLocation, types: [
            .featurePoint,
            .estimatedHorizontalPlane,
            .estimatedVerticalPlane,
            .existingPlane,
            .existingPlaneUsingExtent,
            .existingPlaneUsingGeometry
        ])
        if arHitResults.count != 0 {
            var results = [AnyHashable](repeating: 0, count: arHitResults.count) as? [[AnyHashable: Any]]
            for r in arHitResults {
                results?.append(getDict(from: r)!)
            }
            channel.invokeMethod("onARTap", arguments: results)
        }
    }
    
    @objc private func handlePinch(from recognizer: UIPinchGestureRecognizer?) {
        guard let recognizer = recognizer else {
            return
        }
        if recognizer.state == .changed {
            guard let sceneView = recognizer.view as? ARSCNView else {
                return
            }
            let touchLocation = recognizer.location(in: sceneView)
            let hitResults = sceneView.hitTest(touchLocation , options: [:])
            guard var results = [AnyHashable](repeating: 0, count: hitResults.count ) as? [[AnyHashable: Any]] else {
                return
            }
            for r in hitResults {
                if let name = r.node.name {
                    results.append([
                        "name": name,
                        "scale": Float(recognizer.scale)
                    ])
                }
            }
            if results.count != 0 {
                channel.invokeMethod("onNodePinch", arguments: results)
            }
            recognizer.scale = 1
        }
    }
    
    
    @objc private func handlePan(from recognizer: UIPanGestureRecognizer?) {
        guard let recognizer = recognizer else {
            return
        }
        if recognizer.state == .changed {
            guard let sceneView = recognizer.view as? ARSCNView else {
                return
            }
            let touchLocation = recognizer.location(in: sceneView)
            let translation = recognizer.translation(in: sceneView)
            let hitResults = sceneView.hitTest(touchLocation, options: [:])
            guard var results = [AnyHashable](repeating: 0, count: hitResults.count) as? [[AnyHashable: Any]] else {
                return
            }
            for r in hitResults {
                if let name = r.node.name {
                    results.append([
                        "name": name,
                        "x": Float(translation.x),
                        "y": Float(translation.y)
                    ])
                }
            }
            if results.count != 0 {
                channel.invokeMethod("onNodePan", arguments: results)
            }
        }
    }
    
    private func updatePosition(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        node.position = DecodableUtils.parseVector3(arguments)
        result(nil)
    }
    
    private func updateRotation(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        node.rotation = DecodableUtils.parseVector4(arguments)
        result(nil)
    }
    
    private func updateEulerAngles(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        node.eulerAngles = DecodableUtils.parseVector3(arguments)
        result(nil)
    }
    
    private func updateScale(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        node.scale = DecodableUtils.parseVector3(arguments)
        result(nil)
    }
    
    private func updateSingleProperty(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        guard let keyProperty = arguments["keyProperty"] as? String else {
            return
        }
        guard var object = node.value(forKey: keyProperty) as? [AnyHashable : Any] else {
            return
        }
        guard let propertyName = arguments["propertyName"] as? AnyHashable else {
            return
        }
        object[propertyName] = arguments["propertyValue"]
        result(nil)
    }
    
    private func updateMaterials(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        guard let geometry = GeometryBuilder.createGeometry(arguments, withDevice: sceneView?.device as? NSObject) else {
            return
        }
        node.geometry = geometry
        result(nil)
    }
    
    private func updateFaceGeometry(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let name = arguments["name"] as? String else {
            return
        }
        guard let node = sceneView.scene.rootNode.childNode(withName: name, recursively: true) else {
            return
        }
        guard let geometry = node.geometry as? ARSCNFaceGeometry else {
            return
        }
        guard let anchors = sceneView.session.currentFrame?.anchors else {
            return
        }
        guard let faceAnchor = findAnchor(arguments["fromAnchorId"] as? String, inArray: anchors) else {
            return
        }
        geometry.update(from: faceAnchor.geometry)
        result(nil)
    }
    
    private func findAnchor(_ searchUUID: String?, inArray array: [ARAnchor]?) -> ARFaceAnchor? {
        guard let array = array else {
            return nil
        }
        for obj in array {
            if (obj.identifier.uuidString == searchUUID) {
                return obj as? ARFaceAnchor
            }
        }
        return nil
    }
    
    private func onGetLightEstimate(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let frame = sceneView.session.currentFrame else {
            return
        }
        guard let lightEstimate = frame.lightEstimate else {
            return
        }
        let res = [
            "ambientIntensity": Float(lightEstimate.ambientIntensity),
            "ambientColorTemperature": Float(lightEstimate.ambientColorTemperature)
        ]
        result(res)
    }
    
    private func onProjectPoint(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let point = arguments["point"] as? [AnyHashable: Any] else {
            return
        }
        let vector = DecodableUtils.parseVector3(point)
        let projectedPoint = sceneView.projectPoint(vector)
        let coded = CodableUtils.convertSimdFloat3(toString: SIMD3<Float>.init(projectedPoint))
        result(coded)
    }
    
    private func onPlayAnimation(_ call: FlutterMethodCall?, andResult result: FlutterResult) {
        guard let arguments = call?.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let key = arguments["key"] as? String else {
            return
        }
        guard let sceneName = arguments["sceneName"] as? String else {
            return
        }
        guard let animationIdentifier = arguments["animationIdentifier"] as? String else {
            return
        }
        guard let sceneURL = Bundle.main.url(forResource: sceneName, withExtension: "dae") else {
            return
        }
        guard let sceneSource = SCNSceneSource(url: sceneURL, options: nil) else {
            return
        }
        guard let animationObject = sceneSource.entryWithIdentifier(animationIdentifier, withClass: CAAnimation.self) else {
            return
        }
        animationObject.repeatCount = 1
        animationObject.fadeInDuration = 1
        animationObject.fadeOutDuration = 0.5
        sceneView.scene.rootNode.addAnimation(animationObject, forKey: key)
        result(nil)
    }
    
    private func onStopAnimation(_ call: FlutterMethodCall!, andResult result: FlutterResult) {
        guard let arguments = call.arguments as? [AnyHashable: Any] else {
            return
        }
        guard let key = arguments["key"] as? String else {
            return
        }
        sceneView.scene.rootNode.removeAnimation(forKey: key, blendOutDuration: CGFloat(0.5))
        result(nil)
    }
    
    
    private func getPlane(with number: Int) -> ARWorldTrackingConfiguration.PlaneDetection {
        if number == 0 {
            return []
        } else if number == 1 {
            return ARWorldTrackingConfiguration.PlaneDetection.horizontal
        }
        return ARWorldTrackingConfiguration.PlaneDetection.vertical
    }
    
    private func getWorldAlignment(from number: Int) -> ARConfiguration.WorldAlignment {
        if number == 0 {
            return ARConfiguration.WorldAlignment.gravity
        } else if number == 1 {
            return ARConfiguration.WorldAlignment.gravityAndHeading
        }
        return ARConfiguration.WorldAlignment.camera
    }
    
    private func getNode(with geometry: SCNGeometry?, fromDict dict: [AnyHashable: Any]?) -> SCNNode? {
        var node: SCNNode?
        if (dict?["dartType"] as? String == "ARKitNode") {
            node = SCNNode(geometry: geometry)
        } else if (dict?["dartType"] as? String == "ARKitReferenceNode") {
            let url = dict?["url"] as? String
            let referenceURL = Bundle.main.url(forResource: url, withExtension: nil)
            if let referenceURL = referenceURL {
                node = SCNReferenceNode(url: referenceURL)
            }
            (node as? SCNReferenceNode)?.load()
        } else {
            return nil
        }
        node?.position = DecodableUtils.parseVector3(dict?["position"] as? [AnyHashable: Any])
        
        if dict?["scale"] != nil {
            node?.scale = DecodableUtils.parseVector3(dict?["scale"] as? [AnyHashable: Any])
        }
        if dict?["rotation"] != nil {
            node?.rotation = DecodableUtils.parseVector4(dict?["rotation"] as? [AnyHashable: Any])
        }
        if dict?["name"] != nil {
            node?.name = dict?["name"] as? String
        }
        if dict?["physicsBody"] != nil {
            let physics = dict?["physicsBody"] as? [AnyHashable: Any]
            node?.physicsBody = getPhysicsBody(fromDict: physics)
        }
        if dict?["light"] != nil {
            let light = dict?["light"] as? [AnyHashable: Any]
            node?.light = getLight(from: light)
        }
        
        let renderingOrder = dict?["renderingOrder"] as? NSNumber
        node?.renderingOrder = renderingOrder?.intValue ?? 0
        
        return node
    }
    
    private func getPhysicsBody(fromDict dict: [AnyHashable: Any]?) -> SCNPhysicsBody? {
        let type = dict?["type"] as? NSNumber
        
        var shape: SCNPhysicsShape?
        if dict?["shape"] != nil {
            let shapeDict = dict?["shape"] as? [AnyHashable: Any]
            if shapeDict?["geometry"] != nil {
                let geometry = GeometryBuilder.createGeometry(shapeDict?["geometry"] as? [AnyHashable: Any], withDevice: sceneView?.device as? NSObject)
                shape = SCNPhysicsShape(geometry: geometry!, options: nil)
            }
        }
        
        var physicsBody: SCNPhysicsBody? = nil
        if let int = SCNPhysicsBodyType(rawValue: type?.intValue ?? 0) {
            physicsBody = SCNPhysicsBody(type: int, shape: shape)
        }
        if dict?["categoryBitMask"] != nil {
            let mask = dict?["categoryBitMask"] as? NSNumber
            physicsBody?.categoryBitMask = Int(mask?.uintValue ?? 0)
        }
        
        return physicsBody
    }
    
    private func getLight(from dict: [AnyHashable: Any]?) -> SCNLight? {
        guard let dict = dict else {
            return nil
        }
        let light = SCNLight()
        if dict["type"] != nil {
            var lightType: SCNLight.LightType?
            let type = (dict["type"] as? NSNumber)?.intValue ?? 0
            switch type {
            case 0:
                lightType = .ambient
            case 1:
                lightType = .omni
                break
            case 2:
                lightType = .directional
                break
            case 3:
                lightType = .spot
                break
            case 4:
                lightType = .IES
                break
            case 5:
                lightType = .probe
                break
            default:
                break
            }
            if let lightType = lightType {
                light.type = lightType
            }
        }
        if dict["temperature"] != nil {
            guard let temperature = dict["temperature"] as? NSNumber else {
                return nil
            }
            light.temperature = CGFloat(temperature.floatValue)
        }
        if dict["intensity"] != nil {
            guard let intensity = dict["intensity"] as? NSNumber else {
                return nil
            }
            light.intensity = CGFloat(intensity.floatValue)
        }
        if dict["spotInnerAngle"] != nil {
            guard let spotInnerAngle = dict["spotInnerAngle"] as? NSNumber else {
                return nil
            }
            light.spotInnerAngle = CGFloat(spotInnerAngle.floatValue)
        }
        if dict["spotOuterAngle"] != nil {
            guard let spotOuterAngle = dict["spotOuterAngle"] as? NSNumber else {
                return nil
            }
            light.spotOuterAngle = CGFloat(spotOuterAngle.floatValue)
        }
        if dict["color"] != nil {
            guard let color = dict["color"] as? NSNumber else {
                return nil
            }
            guard let toto = UIColor.fromRGB(color.intValue) else {
                return nil
            }
            light.color = toto
        }
        return light
    }
    
    private func addNodeToScene(with geometry: SCNGeometry?, andCall call: FlutterMethodCall?, andResult result: FlutterResult) {
        let arguments = call?.arguments as? [AnyHashable: Any]
        let node = getNode(with: geometry, fromDict: arguments)
        if arguments?["parentNodeName"] != nil {
            let parentNode = sceneView.scene.rootNode.childNode(withName: arguments?["parentNodeName"] as? String ?? "", recursively: true)
            if let node = node {
                parentNode?.addChildNode(node)
            }
        } else {
            if node != nil {
                sceneView.scene.rootNode.addChildNode(node!)
            }
        }
        result(nil)
    }
    
    private func getDebugOptions(_ arguments: [AnyHashable: Any]?) -> SCNDebugOptions {
        var debugOptions = SCNDebugOptions()
        if (arguments?["showFeaturePoints"] as? NSNumber)?.boolValue ?? false {
            debugOptions.insert(SCNDebugOptions.showFeaturePoints)
        }
        if (arguments?["showWorldOrigin"] as? NSNumber)?.boolValue ?? false {
            debugOptions.insert(SCNDebugOptions.showWorldOrigin)
        }
        return debugOptions
    }
    
    private func getDict(from hit: ARHitTestResult?) -> [AnyHashable: Any]? {
        guard let hit = hit else {
            return nil
        }
        var dict = [
            "type": NSNumber(value: hit.type.rawValue),
            "distance": NSNumber(value: Float(hit.distance)),
            "localTransform": CodableUtils.convertSimdFloat4x4(toString: hit.localTransform),
            "worldTransform": CodableUtils.convertSimdFloat4x4(toString: hit.worldTransform)
            ] as [String : Any]
        if let hitAnchor = hit.anchor {
            dict["anchor"] = CodableUtils.convertARAnchor(toDictionary: hitAnchor)
        }
        return dict
    }
}
