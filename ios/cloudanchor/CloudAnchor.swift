import Flutter

class CloudAnchor: NSObject, FlutterPlugin {
    
    static var registrar: FlutterPluginRegistrar!
    
    static func register(with registrar: FlutterPluginRegistrar) {
        self.registrar = registrar
        let arkitFactory = CloudAnchorFactory(messenger: registrar.messenger())
        registrar.register(arkitFactory, withId: "cloudanchor")
    }
}
